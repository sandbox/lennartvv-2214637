<?php
/**
 * @file
 * Commerce Points rules integration file.
 */

/**
 * Implements hook_rules_event_info().
 */
function commerce_points_rules_event_info() {
  $events = array();

  $events['commerce_points_order_ratio_saved'] = array(
    'label' => t('Order ratio is saved'),
    'group' => t('Commerce Points'),
    'variables' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'bundle' => 'commerce_order',
        'label' => t('Order'),
      ),
    ),
    'access callback' => 'commerce_order_rules_access',
  );
  $events['commerce_points_line_items_updated'] = array(
    'label' => t('Line items are updated based on order ratio'),
    'group' => t('Commerce Points'),
    'variables' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'bundle' => 'commerce_order',
        'label' => t('Order'),
      ),
    ),
    'access callback' => 'commerce_order_rules_access',
  );

  return $events;
}

/**
 * Implements hook_rules_action_info().
 */
function commerce_points_rules_action_info() {
  $actions = array();

  // Prepare an array of arithmetical actions for altering points prices.
  $action_types = array(
    'commerce_points_unit_price_amount' => array(
      'label' => t('Set the points unit price to a specific amount'),
      'amount description' => t('Specify the numeric amount to set the price to.'),
    ),
  );

  // Define the action using a common set of parameters.
  foreach ($action_types as $key => $value) {
    $actions[$key] = array(
      'label' => $value['label'],
      'parameter' => array(
        'commerce_line_item' => array(
          'type' => 'commerce_line_item',
          'label' => t('Line item'),
        ),
        'amount' => array(
          'type' => 'decimal',
          'label' => t('Amount'),
          'description' => $value['amount description'],
        ),
        'component_name' => array(
          'type' => 'text',
          'label' => t('Price component type'),
          'description' => t('Price components track changes to prices made during the price calculation process, and they are carried over from the unit price to the total price of a line item. When an order total is calculated, it combines all the components of every line item on the order. When the unit price is altered by this action, the selected type of price component will be added to its data array and reflected in the order total display when it is formatted with components showing. Defaults to base price, which displays as the order Subtotal.'),
          'options list' => 'commerce_points_price_component_options_list',
          'default value' => 'base_price',
        ),
        'round_mode' => array(
          'type' => 'integer',
          'label' => t('Price rounding mode'),
          'description' => t('Round the resulting price amount after performing this operation.'),
          'options list' => 'commerce_round_mode_options_list',
          'default value' => COMMERCE_ROUND_HALF_UP,
        ),
      ),
      'group' => t('Commerce Points'),
    );
  }

  return $actions;
}

/**
 * Rules action: set the points unit price to a specific amount.
 */
function commerce_points_unit_price_amount($line_item, $amount, $component_name, $round_mode) {
  if (is_numeric($amount)) {
    $wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
    $unit_price = commerce_price_wrapper_value($wrapper, 'commerce_points_unit_price', TRUE);

    // Calculate the updated amount and create a price array representing the
    // difference between it and the current amount.
    $current_amount = $unit_price['amount'];
    $updated_amount = commerce_round($round_mode, $amount);

    $difference = array(
      'amount' => $updated_amount - $current_amount,
      'currency_code' => $unit_price['currency_code'],
      'data' => array(),
    );

    // Set the amount of the unit price and add the difference as a component.
    $wrapper->commerce_points_unit_price->amount = $updated_amount;

    $wrapper->commerce_points_unit_price->data = commerce_price_component_add(
      $wrapper->commerce_points_unit_price->value(),
      $component_name,
      $difference,
      TRUE
    );
  }
}

/**
 * Options list callback: price component selection list.
 */
function commerce_points_price_component_options_list() {
  return commerce_price_component_titles();
}
