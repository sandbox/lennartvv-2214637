<?php
/**
 * @file
 * Export Drupal Commerce orders to Views.
 */

/**
 * Implements hook_views_data()
 */
function commerce_points_views_data() {
  $data = array();

  // Define a handler for an area used to summarize a set of line items.
  $data['commerce_order']['order_total_points'] = array(
    'title' => t('Order total points'),
    'help' => t('Displays the order total points field formatted with its components list; requires an Order ID argument.'),
    'area' => array(
      'handler' => 'commerce_points_handler_area_order_total_points',
    ),
  );

  return $data;
}
