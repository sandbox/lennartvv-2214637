<?php
/**
 * Rule exported to code: rules_cp_points_mode_calculate_line_item_prices_by_ratio.
 */
$rule =
  '{ "rules_cp_points_mode_calculate_line_item_prices_by_ratio" : {
      "LABEL" : "CP: Points mode: calculate line item prices by ratio",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [
        "php",
        "rules",
        "rules_conditional",
        "rules_condition_site_variable",
        "commerce_points",
        "commerce_line_item"
      ],
      "USES VARIABLES" : { "commerce_line_item" : { "label" : "Commerce Line Item", "type" : "commerce_line_item" } },
      "IF" : [
        { "php_eval" : { "code" : "\/\/ This rule only applies to the \u0027points\u0027 mode.\r\nif (variable_get(\u0027commerce_points_mode\u0027, \u0027money\u0027) == \u0027points\u0027) {\r\n  return TRUE;\r\n}" } },
        { "entity_is_of_bundle" : {
            "entity" : [ "commerce-line-item" ],
            "type" : "commerce_line_item",
            "bundle" : { "value" : { "product" : "product" } }
          }
        },
        { "entity_is_of_bundle" : {
            "entity" : [ "commerce-line-item:order" ],
            "type" : "commerce_order",
            "bundle" : { "value" : { "commerce_order" : "commerce_order" } }
          }
        },
        { "data_is" : {
            "data" : [ "commerce-line-item:commerce-points-unit-price-ori:amount-decimal" ],
            "op" : "\u003E",
            "value" : {
              "select" : "site:current-cart-order:order-id",
              "php" : { "code" : "$minimum_points_required = variable_get(\u0027commerce_points_min_points_per_product\u0027, 0);\r\nif (!$minimum_points_required) {\r\n  return \u00270.00\u0027;\r\n}\r\nelse {\r\n  return $minimum_points_required;\r\n}" }
            }
          }
        }
      ],
      "DO" : [
        { "variable_add" : {
            "USING" : { "type" : "decimal", "value" : "100" },
            "PROVIDE" : { "variable_added" : { "order_ratio" : "Order ratio" } }
          }
        },
        { "CONDITIONAL" : [
            {
              "IF" : { "rules_condition_site_variable_compare_variable" : {
                  "variable_name" : "commerce_points_slider_level",
                  "variable_value" : "product"
                }
              },
              "DO" : [
                { "CONDITIONAL" : [
                    {
                      "IF" : { "data_is" : {
                          "data" : [ "commerce-line-item:commerce-points-money-ratio" ],
                          "op" : "\u003E",
                          "value" : "-1"
                        }
                      },
                      "DO" : [
                        { "data_set" : {
                            "data" : [ "order-ratio" ],
                            "value" : [ "commerce-line-item:commerce-points-money-ratio" ]
                          }
                        }
                      ]
                    }
                  ]
                }
              ]
            },
            {
              "ELSE IF" : { "rules_condition_site_variable_compare_variable" : {
                  "variable_name" : "commerce_points_slider_level",
                  "variable_value" : "order"
                }
              },
              "DO" : [
                { "CONDITIONAL" : [
                    {
                      "IF" : { "data_is" : {
                          "data" : [ "commerce-line-item:order:commerce-points-money-ratio" ],
                          "op" : "\u003E",
                          "value" : "-1"
                        }
                      },
                      "DO" : [
                        { "data_set" : {
                            "data" : [ "order-ratio" ],
                            "value" : [ "commerce-line-item:order:commerce-points-money-ratio" ]
                          }
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        { "variable_add" : {
            "USING" : {
              "type" : "decimal",
              "value" : {
                "select" : "site:current-user:uid",
                "php" : { "code" : "$minimum_points_required = variable_get(\u0027commerce_points_min_points_per_product\u0027, 0);\r\nif (!$minimum_points_required) {\r\n  return \u00270.00\u0027;\r\n}\r\nelse {\r\n  return $minimum_points_required;\r\n}" }
              }
            },
            "PROVIDE" : { "variable_added" : { "minimum_points_required" : "Minimum points required" } }
          }
        },
        { "data_convert" : {
            "USING" : {
              "type" : "decimal",
              "value" : [ "commerce-line-item:commerce-points-unit-price-ori:amount" ]
            },
            "PROVIDE" : { "conversion_result" : { "original_points_price_decimal" : "Original points price decimal" } }
          }
        },
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "original-points-price-decimal" ],
              "op" : "-",
              "input_2" : [ "minimum-points-required" ]
            },
            "PROVIDE" : { "result" : { "adjustable_unit_price_points" : "Adjustable total unit price points" } }
          }
        },
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "adjustable-unit-price-points" ],
              "op" : "\/",
              "input_2" : "100"
            },
            "PROVIDE" : { "result" : { "points_unit_price_divided" : "Points unit price divided" } }
          }
        },
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "points-unit-price-divided" ],
              "op" : "*",
              "input_2" : [ "order-ratio" ]
            },
            "PROVIDE" : { "result" : { "adjustable_points_unit_price_by_ratio" : "Adjustable points unit price by ratio" } }
          }
        },
        { "data_convert" : {
            "USING" : {
              "type" : "integer",
              "value" : [ "adjustable-points-unit-price-by-ratio" ]
            },
            "PROVIDE" : { "conversion_result" : { "adjustable_points_unit_price_by_ratio_rounded_down" : "Adjustable points unit price by ratio rounded down" } }
          }
        },
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "adjustable-points-unit-price-by-ratio-rounded-down" ],
              "op" : "+",
              "input_2" : [ "minimum-points-required" ]
            },
            "PROVIDE" : { "result" : { "points_unit_price_by_ratio" : "Points unit price by ratio" } }
          }
        },
        { "commerce_points_unit_price_amount" : {
            "commerce_line_item" : [ "commerce-line-item" ],
            "amount" : [ "points-unit-price-by-ratio" ],
            "component_name" : "base_price",
            "round_mode" : "2"
          }
        },
        { "data_set" : {
            "data" : [ "commerce-line-item:commerce-points-unit-price:currency-code" ],
            "value" : "PNT"
          }
        },
        { "variable_add" : {
            "USING" : {
              "type" : "decimal",
              "value" : {
                "select" : "site:current-user:uid",
                "php" : { "code" : "\/\/ The result is multiplied because price fields expect this format.\r\nreturn (variable_get(\u0027commerce_points_point_value\u0027, 1.00) * 100);" }
              }
            },
            "PROVIDE" : { "variable_added" : { "point_value" : "Point value" } }
          }
        },
        { "variable_add" : {
            "USING" : {
              "type" : "decimal",
              "value" : [ "commerce-line-item:commerce-points-unit-price-ori:amount-decimal" ]
            },
            "PROVIDE" : { "variable_added" : { "original_points_unit_price" : "Orininal points unit price" } }
          }
        },
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "original-points-unit-price" ],
              "op" : "-",
              "input_2" : [ "points-unit-price-by-ratio" ]
            },
            "PROVIDE" : { "result" : { "points_difference" : "Points difference" } }
          }
        },
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "points-difference" ],
              "op" : "*",
              "input_2" : [ "point-value" ]
            },
            "PROVIDE" : { "result" : { "unit_price_by_ratio" : "Unit price by ratio" } }
          }
        },
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "unit-price-by-ratio" ],
              "op" : "+",
              "input_2" : [ "commerce-line-item:commerce-unit-price-ori:amount" ]
            },
            "PROVIDE" : { "result" : { "unit_price_by_ratio_plus_fixed_amount" : "Unit price by ratio plus fixed amount" } }
          }
        },
        { "commerce_line_item_unit_price_amount" : {
            "commerce_line_item" : [ "commerce_line_item" ],
            "amount" : [ "unit-price-by-ratio-plus-fixed-amount" ],
            "component_name" : "base_price",
            "round_mode" : "1"
          }
        }
      ]
    }
  }';
