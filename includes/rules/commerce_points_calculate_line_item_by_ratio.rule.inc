<?php
/**
 * Rule exported to code: rules_commerce_points_calculate_line_item_by_ratio.
 */
$rule =
  '{ "rules_commerce_points_calculate_line_item_by_ratio" : {
      "LABEL" : "Commerce Points: calculate line item by ratio",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "1",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "commerce_product_reference" ],
      "ON" : { "commerce_product_calculate_sell_price" : [] },
      "DO" : [
        { "component_rules_cp_copy_product_prices_to_line_item" : { "commerce_line_item" : [ "commerce_line_item" ] } },
        { "component_rules_cp_points_mode_calculate_line_item_prices_by_ratio" : { "commerce_line_item" : [ "commerce_line_item" ] } },
        { "component_rules_cp_money_mode_calculate_line_item_prices_by_ratio" : { "commerce_line_item" : [ "commerce_line_item" ] } },
        { "component_rules_cp_calculate_and_set_line_item_totals" : { "commerce_line_item" : [ "commerce_line_item" ] } }
      ]
    }
  }';
