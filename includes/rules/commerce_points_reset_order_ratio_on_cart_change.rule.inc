<?php
/**
 * Rule exported to code: rules_commerce_points_reset_order_ratio_on_cart_change.
 */
$rule =
  '{ "rules_commerce_points_reset_order_ratio_on_cart_change" : {
      "LABEL" : "Commerce Points: reset order ratio on cart change",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "commerce_cart" ],
      "ON" : {
        "commerce_cart_product_remove" : [],
        "commerce_cart_product_prepare" : []
      },
      "IF" : [
        { "entity_is_of_bundle" : {
            "entity" : [ "commerce-order" ],
            "type" : "commerce_order",
            "bundle" : { "value" : { "commerce_order" : "commerce_order" } }
          }
        }
      ],
      "DO" : [
        { "component_rules_cp_points_mode_reset_order_ratio" : { "commerce_order" : [ "commerce_order" ] } },
        { "component_rules_cp_money_mode_reset_order_ratio" : { "commerce_order" : [ "commerce_order" ] } }
      ]
    }
  }';
