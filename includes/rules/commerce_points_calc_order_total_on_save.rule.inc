<?php
/**
 * Rule exported to code: rules_commerce_points_calc_order_total_on_save.
 */
$rule =
  '{ "rules_commerce_points_calc_order_total_on_save" : {
      "LABEL" : "Commerce Points: calc order total on save",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "entity" ],
      "ON" : { "commerce_order_presave" : [] },
      "DO" : [
        { "component_rules_cp_calculate_and_set_order_totals" : { "commerce_order" : [ "commerce_order" ] } }
      ]
    }
  }';
