<?php
/**
 * Rule exported to code: rules_cp_iteration_calculate_and_get_line_item_totals.
 */
$rule =
  '{ "rules_cp_iteration_calculate_and_get_line_item_totals" : {
      "LABEL" : "CP: Iteration: calculate and get line item totals",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "current_line_item" : { "label" : "Current line item", "type" : "commerce_line_item" },
        "line_item_total_points_provided" : {
          "label" : "Line item total (points) provided",
          "type" : "decimal",
          "parameter" : false
        },
        "line_item_total_provided" : {
          "label" : "Line item total provided",
          "type" : "decimal",
          "parameter" : false
        }
      },
      "IF" : [
        { "entity_is_of_bundle" : {
            "entity" : [ "current-line-item" ],
            "type" : "commerce_line_item",
            "bundle" : { "value" : { "product" : "product" } }
          }
        }
      ],
      "DO" : [
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "current-line-item:quantity" ],
              "op" : "*",
              "input_2" : [ "current-line-item:commerce-points-unit-price:amount" ]
            },
            "PROVIDE" : { "result" : { "line_item_total_points" : "Line item total (points)" } }
          }
        },
        { "data_set" : {
            "data" : [ "line-item-total-points-provided" ],
            "value" : [ "line-item-total-points" ]
          }
        },
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "current-line-item:quantity" ],
              "op" : "*",
              "input_2" : [ "current-line-item:commerce-unit-price:amount" ]
            },
            "PROVIDE" : { "result" : { "line_item_total" : "Line item total" } }
          }
        },
        { "data_set" : {
            "data" : [ "line-item-total-provided" ],
            "value" : [ "line-item-total" ]
          }
        }
      ],
      "PROVIDES VARIABLES" : [ "line_item_total_points_provided", "line_item_total_provided" ]
    }
  }';
