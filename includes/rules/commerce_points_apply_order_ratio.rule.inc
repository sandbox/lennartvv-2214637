<?php
/**
 * Rule exported to code: rules_commerce_points_apply_order_ratio.
 */
$rule =
  '{ "rules_commerce_points_apply_order_ratio" : {
      "LABEL" : "Commerce Points: apply order ratio",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "rules_conditional", "commerce_points" ],
      "ON" : { "commerce_points_order_ratio_saved" : [] },
      "DO" : [
        { "data_set" : {
            "data" : [ "commerce-order:commerce-points-money-ratio" ],
            "value" : [ "commerce-order:commerce-points-money-ratio" ]
          }
        },
        { "LOOP" : {
            "USING" : { "list" : [ "commerce-order:commerce-line-items" ] },
            "ITEM" : { "current_line_item" : "Current line item" },
            "DO" : [
              { "CONDITIONAL" : [
                  {
                    "IF" : { "NOT data_is_empty" : { "data" : [ "current-line-item" ] } },
                    "DO" : [
                      { "CONDITIONAL" : [
                          {
                            "IF" : { "entity_is_of_bundle" : {
                                "entity" : [ "current-line-item" ],
                                "type" : "commerce_line_item",
                                "bundle" : { "value" : { "product" : "product" } }
                              }
                            },
                            "DO" : [
                              { "component_rules_cp_points_mode_calculate_line_item_prices_by_ratio" : { "commerce_line_item" : [ "current-line-item" ] } },
                              { "component_rules_cp_money_mode_calculate_line_item_prices_by_ratio" : { "commerce_line_item" : [ "current-line-item" ] } },
                              { "component_rules_cp_calculate_and_set_line_item_totals" : { "commerce_line_item" : [ "current-line-item" ] } }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          }
        },
        { "component_rules_cp_calculate_and_set_order_totals" : { "commerce_order" : [ "commerce_order" ] } }
      ]
    }
  }';
