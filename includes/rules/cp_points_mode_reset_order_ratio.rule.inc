<?php
/**
 * Rule exported to code: rules_cp_points_mode_reset_order_ratio.
 */
$rule =
  '{ "rules_cp_points_mode_reset_order_ratio" : {
      "LABEL" : "CP: Points mode: reset order ratio",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "php", "rules" ],
      "USES VARIABLES" : { "commerce_order" : { "label" : "Commerce Order", "type" : "commerce_order" } },
      "IF" : [
        { "php_eval" : { "code" : "\/\/ This rule only applies to the \u0027points\u0027 mode.\r\nif (variable_get(\u0027commerce_points_mode\u0027, \u0027money\u0027) == \u0027points\u0027) {\r\n  return TRUE;\r\n}" } },
        { "entity_is_of_bundle" : {
            "entity" : [ "commerce-order" ],
            "type" : "commerce_order",
            "bundle" : { "value" : { "commerce_order" : "commerce_order" } }
          }
        }
      ],
      "DO" : [
        { "data_set" : {
            "data" : [ "commerce-order:commerce-points-money-ratio" ],
            "value" : "100"
          }
        }
      ]
    }
  }';
