<?php
/**
 * Rule exported to code: rules_cp_copy_product_prices_to_line_item.
 */
$rule =
  '{ "rules_cp_copy_product_prices_to_line_item" : {
      "LABEL" : "CP: copy product prices to line item",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "inline_conditions", "commerce_points" ],
      "USES VARIABLES" : { "commerce_line_item" : { "label" : "Commerce Line Item", "type" : "commerce_line_item" } },
      "IF" : [
        { "entity_is_of_bundle" : {
            "entity" : [ "commerce-line-item" ],
            "type" : "commerce_line_item",
            "bundle" : { "value" : { "product" : "product" } }
          }
        },
        { "commerce_product_contains_products" : {
            "commerce_line_item" : [ "commerce_line_item" ],
            "sku" : [ "commerce-line-item:line-item-label" ]
          }
        },
        { "entity_is_of_bundle" : {
            "entity" : [ "commerce-line-item:commerce-product" ],
            "type" : "commerce_product",
            "bundle" : { "value" : { "product" : "product" } }
          }
        }
      ],
      "DO" : [
        { "commerce_points_unit_price_amount" : {
            "commerce_line_item" : [ "commerce_line_item" ],
            "amount" : [ "commerce-line-item:commerce-product:commerce-points-price:amount" ],
            "component_name" : "base_price",
            "round_mode" : "1"
          }
        },
        { "data_set" : {
            "data" : [ "commerce-line-item:commerce-points-unit-price-ori" ],
            "value" : [ "commerce-line-item:commerce-product:commerce-points-price" ]
          }
        },
        { "data_set" : {
            "data" : [ "commerce-line-item:commerce-unit-price-ori" ],
            "value" : [ "commerce-line-item:commerce-product:commerce-price" ]
          }
        }
      ]
    }
  }';
