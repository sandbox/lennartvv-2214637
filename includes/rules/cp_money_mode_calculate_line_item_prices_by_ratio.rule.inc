<?php
/**
 * Rule exported to code: rules_cp_money_mode_calculate_line_item_prices_by_ratio.
 */
$rule =
  '{ "rules_cp_money_mode_calculate_line_item_prices_by_ratio" : {
      "LABEL" : "CP: Money mode: calculate line item prices by ratio",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "php", "rules", "commerce_line_item", "commerce_points" ],
      "USES VARIABLES" : { "commerce_line_item" : { "label" : "Commerce Line Item", "type" : "commerce_line_item" } },
      "IF" : [
        { "php_eval" : { "code" : "\/\/ This rule only applies to the \u0027money\u0027 mode.\r\nif (variable_get(\u0027commerce_points_mode\u0027, \u0027money\u0027) == \u0027money\u0027) {\r\n  return TRUE;\r\n}" } },
        { "entity_is_of_bundle" : {
            "entity" : [ "commerce-line-item" ],
            "type" : "commerce_line_item",
            "bundle" : { "value" : { "product" : "product" } }
          }
        },
        { "entity_is_of_bundle" : {
            "entity" : [ "commerce-line-item:order" ],
            "type" : "commerce_order",
            "bundle" : { "value" : { "commerce_order" : "commerce_order" } }
          }
        },
        { "data_is" : {
            "data" : [ "commerce-line-item:commerce-unit-price-ori:amount" ],
            "op" : "\u003E",
            "value" : {
              "select" : "site:current-cart-order:order-id",
              "php" : { "code" : "$minimum_money_required = variable_get(\u0027commerce_points_min_money_per_product\u0027, 0);\r\n\/\/ If we don\u0027t print the 0.00, a \u0027zero\u0027 value gives errors.\r\nif (!$minimum_money_required) {\r\n  return \u00270.00\u0027;\r\n}\r\nelse {\r\n  return $minimum_money_required;\r\n}" }
            }
          }
        }
      ],
      "DO" : [
        { "variable_add" : {
            "USING" : {
              "type" : "decimal",
              "value" : {
                "select" : "site:current-user:uid",
                "php" : { "code" : "return (variable_get(\u0027commerce_points_point_value\u0027, 1.00));" }
              }
            },
            "PROVIDE" : { "variable_added" : { "point_value" : "Point value" } }
          }
        },
        { "variable_add" : {
            "USING" : {
              "type" : "decimal",
              "value" : {
                "select" : "site:current-user:uid",
                "php" : { "code" : "$minimum_money_required = variable_get(\u0027commerce_points_min_money_per_product\u0027, 0);\r\n\/\/ If we don\u0027t print the 0.00, a \u0027zero\u0027 value gives errors.\r\nif (!$minimum_money_required) {\r\n  return \u00270.00\u0027;\r\n}\r\nelse {\r\n  return $minimum_money_required;\r\n}" }
              }
            },
            "PROVIDE" : { "variable_added" : { "minimum_money_required" : "Minimum money required" } }
          }
        },
        { "data_convert" : {
            "USING" : {
              "type" : "decimal",
              "value" : [ "commerce-line-item:commerce-unit-price-ori:amount" ]
            },
            "PROVIDE" : { "conversion_result" : { "original_money_price_decimal" : "Original money price decimal" } }
          }
        },
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "original-money-price-decimal" ],
              "op" : "-",
              "input_2" : [ "minimum-money-required" ]
            },
            "PROVIDE" : { "result" : { "adjustable_unit_price" : "Adjustable total unit price" } }
          }
        },
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "adjustable-unit-price" ],
              "op" : "\/",
              "input_2" : "100"
            },
            "PROVIDE" : { "result" : { "unit_price_divided" : "Unit price divided" } }
          }
        },
        { "variable_add" : {
            "USING" : { "type" : "decimal", "value" : "100" },
            "PROVIDE" : { "variable_added" : { "max_scale" : "Max scale" } }
          }
        },
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "max-scale" ],
              "op" : "-",
              "input_2" : [ "commerce-line-item:order:commerce-points-money-ratio" ]
            },
            "PROVIDE" : { "result" : { "reversed_ratio" : "Reversed ratio" } }
          }
        },
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "unit-price-divided" ],
              "op" : "*",
              "input_2" : [ "reversed-ratio" ]
            },
            "PROVIDE" : { "result" : { "adjustable_unit_price_by_ratio" : "Adjustable unit price by ratio" } }
          }
        },
        { "data_calc" : {
            "USING" : { "input_1" : [ "point-value" ], "op" : "*", "input_2" : "100" },
            "PROVIDE" : { "result" : { "point_value_raw" : "Point value (raw)" } }
          }
        },
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "adjustable-unit-price-by-ratio" ],
              "op" : "\/",
              "input_2" : [ "point-value-raw" ]
            },
            "PROVIDE" : { "result" : { "adjustable_unit_price_by_ratio_divided_by_points" : "Adjustable unit price by ratio divided by points" } }
          }
        },
        { "data_convert" : {
            "USING" : {
              "type" : "integer",
              "value" : [ "adjustable-unit-price-by-ratio-divided-by-points" ],
              "rounding_behavior" : "down"
            },
            "PROVIDE" : { "conversion_result" : { "adjustable_unit_price_by_ratio_divided_by_points_int" : "Adjustable unit price by ratio divided by points int" } }
          }
        },
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "adjustable-unit-price-by-ratio-divided-by-points-int" ],
              "op" : "*",
              "input_2" : [ "point-value-raw" ]
            },
            "PROVIDE" : { "result" : { "unit_price_rounded_on_point_value" : "Unit price rounded on point value" } }
          }
        },
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "unit-price-rounded-on-point-value" ],
              "op" : "+",
              "input_2" : [ "minimum-money-required" ]
            },
            "PROVIDE" : { "result" : { "unit_price_by_ratio" : "Unit price by ratio" } }
          }
        },
        { "commerce_line_item_unit_price_amount" : {
            "commerce_line_item" : [ "commerce_line_item" ],
            "amount" : [ "unit-price-by-ratio" ],
            "component_name" : "base_price",
            "round_mode" : "2"
          }
        },
        { "variable_add" : {
            "USING" : {
              "type" : "decimal",
              "value" : [ "commerce-line-item:commerce-unit-price-ori:amount" ]
            },
            "PROVIDE" : { "variable_added" : { "original_unit_price" : "Orininal unit price" } }
          }
        },
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "original-unit-price" ],
              "op" : "-",
              "input_2" : [ "unit-price-by-ratio" ]
            },
            "PROVIDE" : { "result" : { "money_difference_raw" : "Money difference raw" } }
          }
        },
        { "data_calc" : {
            "USING" : { "input_1" : [ "money-difference-raw" ], "op" : "\/", "input_2" : "100" },
            "PROVIDE" : { "result" : { "money_difference" : "Money difference" } }
          }
        },
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "money-difference" ],
              "op" : "\/",
              "input_2" : [ "point-value" ]
            },
            "PROVIDE" : { "result" : { "points_unit_price_by_ratio" : "Points unit price by ratio" } }
          }
        },
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "points-unit-price-by-ratio" ],
              "op" : "+",
              "input_2" : [ "commerce-line-item:commerce-points-unit-price-ori:amount-decimal" ]
            },
            "PROVIDE" : { "result" : { "unit_points_price_by_ratio_plus_fixed_amount" : "Unit points price by ratio plus fixed amount" } }
          }
        },
        { "commerce_points_unit_price_amount" : {
            "commerce_line_item" : [ "commerce_line_item" ],
            "amount" : [ "unit-points-price-by-ratio-plus-fixed-amount" ],
            "component_name" : "base_price",
            "round_mode" : "1"
          }
        },
        { "data_set" : {
            "data" : [ "commerce-line-item:commerce-points-unit-price:currency-code" ],
            "value" : "PNT"
          }
        }
      ]
    }
  }';
