<?php
/**
 * Rule exported to code: rules_cp_calculate_and_set_line_item_totals.
 */
$rule =
  '{ "rules_cp_calculate_and_set_line_item_totals" : {
      "LABEL" : "CP: calculate and set line item totals",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "commerce_line_item" : { "label" : "Commerce Line Item", "type" : "commerce_line_item" } },
      "IF" : [
        { "entity_is_of_bundle" : {
            "entity" : [ "commerce-line-item" ],
            "type" : "commerce_line_item",
            "bundle" : { "value" : { "product" : "product" } }
          }
        }
      ],
      "DO" : [
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "commerce-line-item:quantity" ],
              "op" : "*",
              "input_2" : [ "commerce-line-item:commerce-points-unit-price:amount" ]
            },
            "PROVIDE" : { "result" : { "line_item_total_points" : "Line item total (points)" } }
          }
        },
        { "data_set" : {
            "data" : [ "commerce-line-item:commerce-points-total:amount" ],
            "value" : [ "line-item-total-points" ]
          }
        },
        { "data_set" : {
            "data" : [ "commerce-line-item:commerce-points-total:currency-code" ],
            "value" : "PNT"
          }
        },
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "commerce-line-item:quantity" ],
              "op" : "*",
              "input_2" : [ "commerce-line-item:commerce-unit-price:amount" ]
            },
            "PROVIDE" : { "result" : { "line_item_total" : "Line item total" } }
          }
        },
        { "data_set" : {
            "data" : [ "commerce-line-item:commerce-total:amount" ],
            "value" : [ "line-item-total" ]
          }
        }
      ]
    }
  }';
