<?php
/**
 * Rule exported to code: rules_commerce_points_recalc_prices_on_line_item_change.
 */
$rule =
  '{ "rules_commerce_points_recalc_prices_on_line_item_change" : {
      "LABEL" : "Commerce Points: recalc prices on line item change",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "entity" ],
      "ON" : { "commerce_line_item_update" : [] },
      "DO" : [
        { "component_rules_cp_points_mode_calculate_line_item_prices_by_ratio" : { "commerce_line_item" : [ "commerce_line_item" ] } },
        { "component_rules_cp_money_mode_calculate_line_item_prices_by_ratio" : { "commerce_line_item" : [ "commerce_line_item" ] } },
        { "component_rules_cp_calculate_and_set_line_item_totals" : { "commerce_line_item" : [ "commerce_line_item" ] } },
        { "component_rules_cp_calculate_and_set_order_totals" : { "commerce_order" : [ "commerce-line-item:order" ] } }
      ]
    }
  }';
