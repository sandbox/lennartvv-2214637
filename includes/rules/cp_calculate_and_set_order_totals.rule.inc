<?php
/**
 * Rule exported to code: rules_cp_calculate_and_set_order_totals.
 */
$rule =
  '{ "rules_cp_calculate_and_set_order_totals" : {
      "LABEL" : "CP: calculate and set order totals",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "rules_conditional" ],
      "USES VARIABLES" : { "commerce_order" : { "label" : "Commerce Order", "type" : "commerce_order" } },
      "IF" : [
        { "entity_is_of_bundle" : {
            "entity" : [ "commerce-order" ],
            "type" : "commerce_order",
            "bundle" : { "value" : { "commerce_order" : "commerce_order" } }
          }
        }
      ],
      "DO" : [
        { "variable_add" : {
            "USING" : {
              "type" : "commerce_price",
              "value" : { "value" : { "amount" : 0, "currency_code" : "EUR" } }
            },
            "PROVIDE" : { "variable_added" : { "new_order_total" : "New order total" } }
          }
        },
        { "variable_add" : {
            "USING" : {
              "type" : "commerce_price",
              "value" : { "value" : { "amount" : 0, "currency_code" : "PNT" } }
            },
            "PROVIDE" : { "variable_added" : { "new_order_total_points" : "New order total (points)" } }
          }
        },
        { "LOOP" : {
            "USING" : { "list" : [ "commerce-order:commerce-line-items" ] },
            "ITEM" : { "current_line_item" : "Current line item" },
            "DO" : [
              { "CONDITIONAL" : [
                  {
                    "IF" : { "NOT data_is_empty" : { "data" : [ "current-line-item" ] } },
                    "DO" : [
                      { "CONDITIONAL" : [
                          {
                            "IF" : { "entity_is_of_bundle" : {
                                "entity" : [ "current-line-item" ],
                                "type" : "commerce_line_item",
                                "bundle" : { "value" : { "product" : "product" } }
                              }
                            },
                            "DO" : [
                              { "component_rules_cp_iteration_calculate_and_get_line_item_totals" : {
                                  "USING" : { "current_line_item" : [ "current_line_item" ] },
                                  "PROVIDE" : {
                                    "line_item_total_points_provided" : { "line_item_total_points_provided" : "Line item total (points) provided" },
                                    "line_item_total_provided" : { "line_item_total_provided" : "Line item total provided" }
                                  }
                                }
                              },
                              { "data_calc" : {
                                  "USING" : {
                                    "input_1" : [ "new-order-total:amount" ],
                                    "op" : "+",
                                    "input_2" : [ "line-item-total-provided" ]
                                  },
                                  "PROVIDE" : { "result" : { "temporary_order_total_result" : "Temporary order total result" } }
                                }
                              },
                              { "data_set" : {
                                  "data" : [ "new-order-total:amount" ],
                                  "value" : [ "temporary-order-total-result" ]
                                }
                              },
                              { "data_calc" : {
                                  "USING" : {
                                    "input_1" : [ "new-order-total-points:amount" ],
                                    "op" : "+",
                                    "input_2" : [ "line-item-total-points-provided" ]
                                  },
                                  "PROVIDE" : { "result" : { "temporary_order_total_result_points" : "Temporary order total result (points)" } }
                                }
                              },
                              { "data_set" : {
                                  "data" : [ "new-order-total-points:amount" ],
                                  "value" : [ "temporary-order-total-result-points" ]
                                }
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          }
        },
        { "data_set" : {
            "data" : [ "commerce-order:commerce-order-total:amount" ],
            "value" : [ "new-order-total:amount" ]
          }
        },
        { "data_set" : {
            "data" : [ "commerce-order:commerce-points-order-total:amount" ],
            "value" : [ "new-order-total-points:amount" ]
          }
        },
        { "data_set" : {
            "data" : [ "commerce-order:commerce-points-order-total:currency-code" ],
            "value" : "PNT"
          }
        }
      ]
    }
  }';
