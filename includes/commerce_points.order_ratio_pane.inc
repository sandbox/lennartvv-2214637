<?php
/**
 * @file
 * Defines a commerce pane containing the order ratio slide field.
 *
 * @author Lennart Van Vaerenbergh, 2014
 */

/**
* Implements base_settings_form().
*/
function commerce_points_order_ratio_pane_settings_form($checkout_pane) {
  // The configuration has moved to another page due to the lack of translate
  // options in this config form.
	$form['commerce_points_pane_title'] = array(
	  '#type' => 'item',
    '#markup' => '<p>' . t('The settings of this pane can be modified on the <a href="@settings_url">Commerce Points settings page</a>.', array(
      '@settings_url' => url('admin/commerce/config/commerce_points'),
    )) . '</p>',
  );

  return $form;
}

/**
 * Order ratio pane: form callback.
 */
function commerce_points_order_ratio_pane_checkout_form($form, &$form_state, $checkout_pane, $order) {
  // Save order for use in ajax callback.
  $form_state['order'] = $order;

  // Get pane description.
  $description = variable_get('commerce_points_pane_description', array('value' => '', 'format' => NULL));

  // Get bubble format.
  $bubble_format = variable_get('commerce_points_slider_bubble_format', '[value]');
  $bubble_format = str_replace('[value]', '%{value}%', $bubble_format);

  // Check if this order has content for the points fields (by checking the
  // first line item of the order). If not, the order dates from a time this
  // module was not active and hasn't been updated yet.
  $first_line_item = commerce_line_item_load($order->commerce_line_items[LANGUAGE_NONE][0]['line_item_id']);
  if (isset($first_line_item->commerce_points_unit_price_ori[LANGUAGE_NONE])) {
    $pane_form['commerce_points_description_value'] = array(
      '#type' => 'item',
      '#markup' => check_markup($description['value'], $description['format']),
    );
    $pane_form['price_slider'] = array(
      '#title' => NULL,
      '#type' => 'slider',
      '#default_value' => ($order->commerce_points_money_ratio[LANGUAGE_NONE]) ? $order->commerce_points_money_ratio[LANGUAGE_NONE][0]['value'] : 0,
      '#slider_style' => 'green',
      '#range' => FALSE,
      '#min' => 0,
      '#max' => 100,
      '#step' => variable_get('commerce_points_slider_step_size', 1.00),
      '#display_inputs' => FALSE,
      '#display_bubble' => variable_get('commerce_points_slider_bubble', 1),
      '#display_bubble_format' => $bubble_format,
      '#ajax' => array(
        'callback' => 'commerce_points_slider_callback',
        'effect' => variable_get('commerce_points_reload_animation', 'fade'),
        'progress' => array('type' => 'none'),
      ),
    );

    // Load default money currency.
    $default_currency_code = commerce_default_currency();
    $default_currency_array = commerce_currency_load($default_currency_code);

    // Calculate order total based on product bundles only.
    // This is used to display the money value on the slider.
    $order_subtotal = 0;
    foreach ($order->commerce_line_items[LANGUAGE_NONE] as $line_item) {
      $line_item = commerce_line_item_load($line_item['line_item_id']);
      if (is_object($line_item)) {
        if ($line_item->type == 'product') {
          $order_subtotal += $line_item->commerce_total[LANGUAGE_NONE][0]['amount'];
        }
      }
    }

    // Format the money price without the function commerce_currency_format().
    // This is because we need span tags around the amount, apart from
    // the symbol.
    // Format to decimal amount.
    $money_amount = number_format(commerce_currency_amount_to_decimal($order_subtotal, $default_currency_code), 2);

    // Use decimal separator of default money currency.
    $money_amount = str_replace('.', $default_currency_array['decimal_separator'], $money_amount);
    $money_amount_formatted = '<span class="value">' . $money_amount . '</span>';

    // Add symbol.
    if ($default_currency_array['symbol_placement'] == 'before') {
      // We don't add the symbol spacer, because Commerce doesn't add it
      // anyway when the symbol is placed in front of the amount.
      $decimal_amount_symbol = '<span class="symbol">' . $default_currency_array['symbol'] . '</span>' . $money_amount_formatted;
    }
    else {
      $decimal_amount_symbol =  $money_amount_formatted . '<span class="symbol">' .  $default_currency_array['symbol_spacer'] . $default_currency_array['symbol'] . '</span>';
    }

    // Print the price numbers.
    $pane_form['money_amount'] = array(
      '#type' => 'item',
      '#markup' => $decimal_amount_symbol,
    );
    $pane_form['points_amount'] = array(
      '#type' => 'item',
      '#markup' => '<span class="cp-amount">' . number_format($order->commerce_points_order_total[LANGUAGE_NONE][0]['amount'], 0, '', variable_get('commerce_points_thousand_marker', '')) . '</span> <span class="cp-currency">' . t('!Points', userpoints_translation()) . '</span>',
    );

    // Prepare product information for realtime front end price calculation.
    $line_items_js_array = array();
    foreach ($order->commerce_line_items[LANGUAGE_NONE] as $line_item) {
      $line_item = commerce_line_item_load($line_item['line_item_id']);
      if (is_object($line_item)) {
        if ($line_item->type == 'product') {
          $line_items_js_array[] = array(
            'commerce_unit_price' => $line_item->commerce_unit_price[LANGUAGE_NONE][0]['amount'],
            'commerce_unit_price_ori' => $line_item->commerce_unit_price_ori[LANGUAGE_NONE][0]['amount'],
            'commerce_points_unit_price' => $line_item->commerce_points_unit_price[LANGUAGE_NONE][0]['amount'],
            'commerce_points_unit_price_ori' => $line_item->commerce_points_unit_price_ori[LANGUAGE_NONE][0]['amount'],
            'quantity' => $line_item->quantity,
          );
        }
      }
    }

    // Add javascript variables.
    global $language;
    global $user;
    $js_variables = array(
      'current_language' => $language->language,
      'point_balance' => userpoints_get_current_points($user->uid, 'all'),
      'animation' => variable_get('commerce_points_reload_animation', 'fade'),
      'order_id' => $order->order_id,
      'point_value' => variable_get('commerce_points_point_value', 1),
      'order_ratio' => $order->commerce_points_money_ratio[LANGUAGE_NONE][0]['value'],
      'order_total_points' => $order->commerce_points_order_total[LANGUAGE_NONE][0]['amount'],
      'mode' => variable_get('commerce_points_mode', 'money'),
      'minimum_points_required' => variable_get('commerce_points_min_points_per_product', 0),
      'minimum_money_required' => variable_get('commerce_points_min_money_per_product', 0),
      'decimal_separator' => $default_currency_array['decimal_separator'],
      'thousand_marker' => variable_get('commerce_points_thousand_marker', ''),
      'line_items' => $line_items_js_array,
    );

    // Make variables available for js.
    drupal_add_js(array('commerce_points' => $js_variables), 'setting');
    // Add custom js for realtime value adjustments.
    drupal_add_js(drupal_get_path('module', 'commerce_points') . '/js/sliderfield_events.js', 'file');
    // Add form specific css.
    drupal_add_css(drupal_get_path('module', 'commerce_points') . '/css/commerce_points_style.css', array('group' => CSS_DEFAULT));
  }
  else {
    $pane_form['commerce_points_invalid_order'] = array(
      '#type' => 'item',
      '#title' => t('Price slider not available'),
      '#markup' => '<p>' . t('The points data for this order is not complete. Remove all current products form your cart in order to unlock the price slider.') . '</p>',
    );
  }

  return $pane_form;
}

/**
 * Ajax callback for the order ratio slider.
 */
function commerce_points_slider_callback($form, $form_state) {
  // Act on slider change.
  if (!empty($form_state['order'])) {
    $order = $form_state['order'];

    // Apply order ratio to order.
    $order->commerce_points_money_ratio[LANGUAGE_NONE][0]['value'] = $form_state['input']['commerce_points_order_ratio']['price_slider']['value'];

    // Invoke Rules event.
    rules_invoke_event('commerce_points_order_ratio_saved', $order);

    // Pass the line items to Rules.
    // We use this rules event from commerce_product_pricing to make sure
    // the prices are adjusted before we ajax-reload the cart.
    foreach ($order->commerce_line_items[LANGUAGE_NONE] as $line_item) {
      $line_item_object = commerce_line_item_load($line_item['line_item_id']);

      if (is_object($line_item_object)) {
        if ($line_item_object->type == 'product') {
          rules_invoke_event('commerce_product_calculate_sell_price', commerce_line_item_load($line_item['line_item_id']));
          // Save the line item after the price calculation in Rule. This way the
          // most recent prices can be used for total order price calculation
          // below. For some reason an entity save in Rules does not to the trick.
          commerce_line_item_save(commerce_line_item_load($line_item['line_item_id']));
        }
      }
    }

    // Reload order because it has changed by the previous rule.
    $order_reloaded = commerce_order_load($order->order_id);

    // Recalculate order total 'money' price.
    commerce_order_calculate_total($order_reloaded);
  }

  // AJAX handling.
  // Prepare new cart content to refres the cart.
  $pane_form = array();

  // Extract the View and display keys from the cart contents pane setting.
  list($view_id, $display_id) = explode('|', variable_get('commerce_cart_contents_pane_view', 'commerce_cart_summary|default'));
  $pane_form['cart_contents_view'] = array(
    '#markup' => commerce_embed_view($view_id, $display_id, array($form_state['order']->order_id)),
  );

  // Render output.
  $output = drupal_render($pane_form);

  // Prepare ajax command.
  $commands[] = ajax_command_replace(variable_get('commerce_points_reload_element', '.view-commerce-cart-summary.view-display-id-default'), $output);

  return array('#type' => 'ajax', '#commands' => $commands);
}
