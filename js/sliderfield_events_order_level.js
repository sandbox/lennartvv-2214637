/**
 * @file
 * Javascript for Commerce Points.
 *
 * Updates prices realtime while sliding on order level.
 */
(function ($) {
  
  // Global var to indicate if AJAX is loading or not.
  var loading = false;

  Drupal.behaviors.sliderfield_attach = {
    attach: function (context, settings) {

      $('#edit-commerce-points-order-ratio-price-slider-container', context).once(function() {
        // Get variables from PHP.
  	    var current_language = Drupal.settings.commerce_points.current_language;
  	    var current_point_balance = Drupal.settings.commerce_points.point_balance;
  	    var animation = Drupal.settings.commerce_points.animation;
  	    var order_id = Drupal.settings.commerce_points.order_id;
  	    var point_value = Drupal.settings.commerce_points.point_value;
  	    var order_ratio = Drupal.settings.commerce_points.order_ratio;
  	    var order_total_points = Drupal.settings.commerce_points.order_total_points;
  	    var mode = Drupal.settings.commerce_points.mode;
  	    var minimum_points_required = Drupal.settings.commerce_points.minimum_points_required;
  	    var minimum_money_required = Drupal.settings.commerce_points.minimum_money_required;
  	    var decimal_separator = Drupal.settings.commerce_points.decimal_separator;
  	    var thousand_marker = Drupal.settings.commerce_points.thousand_marker;
  	    var line_items = Drupal.settings.commerce_points.line_items;
  	    
  	    // Initial CSS class depending on sufficient points or not.
  	    if (order_total_points > current_point_balance) {
  	      $('#edit-commerce-points-order-ratio').addClass('insufficient-points');
  	    }
  	    else {
  	      $('#edit-commerce-points-order-ratio').addClass('sufficient-points');
  	    }
      
        // Attach functionality to the slide event to update numbers realtime.
        $(this).bind("slide", function( event, ui ) {
          // Set initial vars.
    	    var slided_money_total = 0.00;
    	    var slided_points_total = 0;
          var order_ratio = ui.value;
        
          // Point mode.
          if (mode == 'points') {
            // Loop through line items.
            for (index = 0; index < line_items.length; ++index) {
              line_item = line_items[index];
              // The part of the points price that can be altered by the slider.
              variable_points_price = line_item['commerce_points_unit_price_ori'] - minimum_points_required;
              // Calculate the points unit price by ratio.
              line_item['commerce_points_unit_price_by_ratio'] = Math.round((variable_points_price / 100) * order_ratio);
              // Add the fixed points if this setting is set.
              line_item['commerce_points_unit_price_by_ratio'] += parseInt(minimum_points_required);
              // If the fixed points are higher than the product price, take
              // the product price as the 'ratio price'.
              if (parseInt(minimum_points_required) >= line_item['commerce_points_unit_price_ori']) {
                line_item['commerce_points_unit_price_by_ratio'] = line_item['commerce_points_unit_price_ori'];
              }
              // Calculate the line item total points price.
              line_item['commerce_points_total_points'] = line_item['commerce_points_unit_price_by_ratio'] * line_item['quantity'];
              // Add to order total.
              slided_points_total += Math.round(line_item['commerce_points_total_points']);
              // Calculate the money unit price by ratio.
              line_item['commerce_points_unit_price_points_difference'] = line_item['commerce_points_unit_price_ori'] - line_item['commerce_points_unit_price_by_ratio'];
              line_item['commerce_unit_price'] = round(line_item['commerce_points_unit_price_points_difference'] * point_value, 2, 'PHP_ROUND_HALF_UP');
              // Add the fixed money price if there is any set.
              line_item['commerce_unit_price'] += line_item['commerce_unit_price_ori'] / 100;
              // Calculate the line item total money price.
              line_item['commerce_points_total_money'] = line_item['commerce_unit_price'] * line_item['quantity'];
              // Add to order total.
              slided_money_total += line_item['commerce_points_total_money'];
            }
          }
          // Money mode.
          if (mode == 'money') {
            // Reverse ratio for the Money mode.
            order_ratio = round(100 - ui.value, 2, 'PHP_ROUND_HALF_UP');
            // Loop through line items.
            for (index = 0; index < line_items.length; ++index) {
              line_item = line_items[index];
              // The part of the money price that can be altered by the slider.
              variable_money_price = line_item['commerce_unit_price_ori'] - minimum_money_required;
              // Calculate the money unit price by ratio.
              line_item['commerce_unit_price_by_ratio'] = (variable_money_price / 100) * order_ratio;
              // The smallest step to in- or decrease money is the value
              // of one point. In our current system the point value is saved as
              // a decimal (!= Commerce raw amount) so we multiply by 100.
              multiplier = Math.floor(line_item['commerce_unit_price_by_ratio'] / (point_value*100));
              line_item['commerce_unit_price_by_ratio'] = multiplier * (point_value * 100);
              // Add the fixed money if this setting is set.
              line_item['commerce_unit_price_by_ratio'] += parseFloat(minimum_money_required);
              // If the minimom required price is higher than the original
              // price, let's take the original price.
              if (parseFloat(minimum_money_required) >= line_item['commerce_unit_price_ori']) {
                line_item['commerce_unit_price_by_ratio'] = line_item['commerce_unit_price_ori'];
              }
              // Calculate the line item total money price. 'Round half down' is
              // also performed in the PHP Rules calculation. It's important these
              // calculations are identical.
              line_item['commerce_points_total_money'] = (round(line_item['commerce_unit_price_by_ratio'] / 100, 2, 'PHP_ROUND_HALF_DOWN')) * line_item['quantity'];
              // Add to order total.
              slided_money_total += line_item['commerce_points_total_money'];
              // Calculate the points unit price by ratio.
              line_item['commerce_points_unit_price_money_difference'] = line_item['commerce_unit_price_ori'] - line_item['commerce_unit_price_by_ratio'];
              line_item['commerce_points_unit_price'] = Math.round((line_item['commerce_points_unit_price_money_difference'] / 100) / point_value);
              // Add the fixed money price if there is any set.
              line_item['commerce_points_unit_price'] += parseInt(line_item['commerce_points_unit_price_ori']);
              // Calculate the line item total points price.
              line_item['commerce_points_total_points'] = Math.round(line_item['commerce_points_unit_price'] * line_item['quantity']);
              // Add to order total.
              slided_points_total += line_item['commerce_points_total_points'];
            }
          }
          // Prepare the HTML values.
          slided_money_total = slided_money_total.toFixed(2);
          slided_money_total = slided_money_total.replace(".", decimal_separator);
          slided_points_total_raw = slided_points_total;
          slided_points_total = addThousandMarker(slided_points_total);
          // Set the HTML values.
          $('#edit-commerce-points-order-ratio-money-amount .value').html(slided_money_total);
          $('#edit-commerce-points-order-ratio-points-amount .cp-amount').html(slided_points_total);
          // Set CSS class depending on sufficient points or not.
    	    if (slided_points_total_raw > current_point_balance) {
    	      $('#edit-commerce-points-order-ratio').removeClass('sufficient-points').addClass('insufficient-points');
    	    }
    	    else {
    	      $('#edit-commerce-points-order-ratio').removeClass('insufficient-points').addClass('sufficient-points');
    	    }
        });
      
        $(this).on("slidechange", function( event, ui ) {
          if (!loading) {
            // Indicate that AJAX is loading.
            loading = true;
            // Disable the slider while AJAX loading.
            $('#edit-commerce-points-order-ratio-price-slider-container').slider( "option", "disabled", true );
            // Slide animation on cart.
            if (animation == 'slide') {
              $('.view-commerce-cart-summary.view-display-id-default').slideUp("slow", function() {
                // Animation complete.
              });
            }
            // Fade anmition on cart.
            else if (animation == 'fade') {
              $('.view-commerce-cart-summary.view-display-id-default').fadeTo("slow", 0, function() {
                // Add ajax loader image.
                $('#edit-cart-contents').append('<div class="commerce-points-ajax-loader"></div>');
                $('.commerce-points-ajax-loader').css({
                  'margin-left': '-110px',
                  'position' : 'absolute',
                  'top' : '50%',
                  'left' : '50%',
                  'display' : 'none'
                });
                $('.commerce-points-ajax-loader').fadeTo('slow', 1);
              });
            }
          }
        });
      
      });
    
      // Remove load bar when cart is AJAX reloaded.
      $('.view-commerce-cart-summary.view-display-id-default').ajaxComplete(function(event, xhr, settings) {
        // Indicate that AJAX stopped loading.
        loading = false;
        // Remove the loading bar.
        $('.commerce-points-ajax-loader').fadeTo("slow", 0, function() {
          $(this).remove();
          // Enable the slider after AJAX reloading.
          // Disable the slider while AJAX loading.
          $('#edit-commerce-points-order-ratio-price-slider-container').slider( "option", "disabled", false );
        });
      });
    }
  };

  function roundDecimal(number, roundType, direction){
    if (roundType == 'floor') {
      return Math.floor(number * 100) / 100;
    }
    else if (roundType == 'round') {
      return Math.round(number * 100) / 100;
    }
    else if (roundType == 'ceil') {
      return Math.ceil(number * 100) / 100;
    }
    else {
      return false;
    }
  }

  function round(value, precision, mode) {
    var m, f, isHalf, sgn; // helper variables
    precision |= 0; // making sure precision is integer
    m = Math.pow(10, precision);
    value *= m;
    sgn = (value > 0) | -(value < 0); // sign of the number
    isHalf = value % 1 === 0.5 * sgn;
    f = Math.floor(value);

    if (isHalf) {
      switch (mode) {
        case 'PHP_ROUND_HALF_DOWN':
          value = f + (sgn < 0); // rounds .5 toward zero
          break;
        case 'PHP_ROUND_HALF_EVEN':
          value = f + (f % 2 * sgn); // rouds .5 towards the next even integer
          break;
        case 'PHP_ROUND_HALF_ODD':
          value = f + !(f % 2); // rounds .5 towards the next odd integer
          break;
        case 'PHP_ROUND_HALF_UP':
          value = f + (sgn > 0); // rounds .5 away from zero
      }
    }

    return (isHalf ? value : Math.round(value)) / m;
  }

  function addThousandMarker(x) {
    var thousand_marker = Drupal.settings.commerce_points.thousand_marker;
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, thousand_marker);
  }

  function removeThousandMarker(x) {
    var thousand_marker = Drupal.settings.commerce_points.thousand_marker;
    return x.replace(".", thousand_marker);
  }

})(jQuery);