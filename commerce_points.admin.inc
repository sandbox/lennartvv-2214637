<?php
/**
 * @file
 * Provides the configuration forms of the module.
 *
 * @author Lennart Van Vaerenbergh, 2014
 */

function commerce_points_settings_form() {
  $form = array();

  // Module settings
  $form['commerce_points_slider_level'] = array(
	  '#type' => 'radios',
	  '#title' => t('Slider acts on'),
		'#options' => array(
		  'product' => t('product level'),
		  'order' => t('order level'),
		),
		'#default_value' => variable_get('commerce_points_slider_level', 'order'),
		'#description' => t('<strong>Product level:</strong> the money-points balance can be set per product. The slider is displayed on the product page.
		<br /><strong>Order level:</strong> the money-points balance can be set per order, equally divided over all products in the order. The slider is displayed on the cart page in a Commerce pane.'),
		'#required' => TRUE,
	);

  $form['commerce_points_general_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['commerce_points_general_settings']['commerce_points_mode'] = array(
	  '#type' => 'radios',
	  '#title' => t('Leading price'),
		'#options' => array(
		  'points' => t('!Points', userpoints_translation()),
		  'money' => t('Money'),
		),
		'#description' => t('<strong>!Points:</strong> All products must contain !points prices. The money price fields are free to fill in and will serve as a minimum compulsory money payment.
		<br /><strong>Money:</strong> All products must contain money prices. The !points price fields are free to fill in and will serve as a minimum compulsory !points payment.
		<br />When editing a product, both the money price (= original commerce price field) and the !points price fields are available to fill in.', userpoints_translation()),
		'#default_value' => variable_get('commerce_points_mode', 'money'),
		'#required' => TRUE,
	);

	$form['commerce_points_general_settings']['commerce_points_slider_step_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Slider step size (out of 100)'),
		'#default_value' => variable_get('commerce_points_slider_step_size', 1.00),
		'#description' => t('The value can be decimal. The smaller the step, the more accurate the slider gets. But watch out, very small steps can cause rounding inaccuracy on prices.<br />
		The total scale of the slider is 100. You current setting allows your slider to have @steps steps.', array('@steps' => 100 / variable_get('commerce_points_slider_step_size', 1.00))),
		'#size' => 8,
		'#required' => TRUE,
  );

	$form['commerce_points_general_settings']['commerce_points_point_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Money value of one !Point', userpoints_translation()),
		'#default_value' => variable_get('commerce_points_point_value', 1.00),
		'#size' => 8,
		'#field_prefix' => t('1 !Point = ', userpoints_translation()),
		'#field_suffix' => commerce_default_currency(),
		'#required' => TRUE,
  );

	$form['commerce_points_general_settings']['commerce_points_min_points_per_product'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum !points to pay per product', userpoints_translation()),
		'#default_value' => variable_get('commerce_points_min_points_per_product', 0),
		'#description' => t('This will be the minimum !points amount to pay per product unit.', userpoints_translation()),
		'#field_suffix' => t('!Points', userpoints_translation()),
		'#size' => 8,
		'#states' => array(
      'visible' => array(
        ':input[name="commerce_points_mode"]' => array('value' => 'points'),
      ),
    ),
    '#required' => TRUE,
  );

  $form['commerce_points_general_settings']['commerce_points_min_money_per_product'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum money to pay per product'),
		'#default_value' => commerce_currency_amount_to_decimal(variable_get('commerce_points_min_money_per_product', 0.00), commerce_default_currency()),
		'#description' => t('This will be the minimum money amount to pay per product unit.'),
		'#field_suffix' => commerce_default_currency(),
		'#size' => 8,
		'#states' => array(
      'visible' => array(
        ':input[name="commerce_points_mode"]' => array('value' => 'money'),
      ),
    ),
    '#required' => TRUE,
  );

  $form['commerce_points_appearance'] = array(
    '#type' => 'fieldset',
    '#title' => t('Slider Appearance'),
    '#description' => t('These settings are applied to the commerce pane containing the slider. The commerce panes can be configured <a href="@panes_url">here</a>.', array(
      '@panes_url' => url('admin/commerce/config/checkout'),
    )),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['commerce_points_appearance']['commerce_points_pane_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Point slider title'),
		'#default_value' => variable_get('commerce_points_pane_title', 'Price slider'),
		'#description' => t('The title displayed above the price slider.'),
		'#size' => 100,
		'#required' => TRUE,
	);

	$default_description = variable_get('commerce_points_pane_description', array('value' => '', 'format' => NULL));
	$form['commerce_points_appearance']['commerce_points_pane_description'] = array(
    '#type' => 'text_format',
    '#format' => $default_description['format'],
    '#title' => t('Point slider description'),
		'#default_value' => $default_description['value'],
		'#description' => t('This text will be displayed in the slider pane right above the slider.'),
  );

  $options = array(
    '' => t('<none>'),
    '.' => t('Decimal point'),
    ',' => t('Comma'),
    ' ' => t('Space'),
  );
  $form['commerce_points_appearance']['commerce_points_thousand_marker'] = array(
    '#type' => 'select',
    '#title' => t('Thousand marker'),
    '#options' => $options,
    '#default_value' => variable_get('commerce_points_thousand_marker', ''),
    '#description' => t('The thousand marker for the points value on the price slider.'),
	);

  $form['commerce_points_appearance']['commerce_points_reload_element'] = array(
    '#type' => 'textfield',
    '#title' => t('HTML element to AJAX reload the cart into (CSS selector)'),
		'#default_value' => variable_get('commerce_points_reload_element', '.view-commerce-cart-summary.view-display-id-default'),
		'#description' => t('This html element will get replaced by the rendered Commerce cart View.'),
		'#size' => 100,
		'#required' => TRUE,
		'#states' => array(
      'visible' => array(
        ':input[name="commerce_points_slider_level"]' => array('value' => 'order'),
      ),
    ),
	);

	$form['commerce_points_appearance']['commerce_points_slider_bubble'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display percentage bubble'),
    '#label' => t('Display percentage bubble'),
    '#default_value' => variable_get('commerce_points_slider_bubble', 1),
	);

	$form['commerce_points_appearance']['commerce_points_slider_bubble_format'] = array(
    '#type' => 'textfield',
    '#title' => t('Bubble format'),
    '#default_value' => variable_get('commerce_points_slider_bubble_format', '[value]'),
    '#states' => array(
      'visible' => array(
        ':input[name="commerce_points_slider_bubble"]' => array('checked' => TRUE),
      ),
    ),
    '#size' => 40,
    '#description' => t('Use [value] to display the slider field value in the bubble.'),
	);

	$form['commerce_points_appearance']['commerce_points_reload_animation'] = array(
	  '#type' => 'radios',
	  '#title' => t('Cart AJAX reload animation'),
		'#options' => array(
		  'none' => t('None'),
		  'slide' => t('Slide'),
		  'fade' => t('Fade'),
		),
		'#default_value' => variable_get('commerce_points_reload_animation', 'fade'),
		'#required' => TRUE,
		'#states' => array(
      'visible' => array(
        ':input[name="commerce_points_slider_level"]' => array('value' => 'order'),
      ),
    ),
	);

	$form['commerce_points_balance_block'] = array(
    '#type' => 'fieldset',
    '#title' => t('Points balance block'),
    '#description' => t('These settings are applied to the Drupal Block containing the points balance.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['commerce_points_balance_block']['commerce_points_balance_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
		'#default_value' => variable_get('commerce_points_balance_label', ''),
		'#description' => t('This label will be prepended to the points balance.'),
		'#size' => 60,
		'#required' => FALSE,
	);

  // Add extra submit handler for changing the default order ratio's initial
  // value.
  $form['#submit'][] = '_commerce_points_adjust_default_ratio';

	return system_settings_form($form);
}

function commerce_points_settings_form_validate($form, &$form_state) {
  // Change possible decimal commas to decimal points.
  $form_state['input']['commerce_points_min_money_per_product'] = str_replace(',', '.', $form_state['input']['commerce_points_min_money_per_product']);
  $form_state['values']['commerce_points_point_value'] = str_replace(',', '.', $form_state['input']['commerce_points_point_value']);
  $form_state['values']['commerce_points_slider_step_size'] = str_replace(',', '.', $form_state['input']['commerce_points_slider_step_size']);
  // Change decimal to raw amount. This raw amount will be used for
  // price calculations.
  $form_state['values']['commerce_points_min_money_per_product'] = commerce_currency_decimal_to_amount($form_state['input']['commerce_points_min_money_per_product'], commerce_default_currency(), TRUE);
}

/**
 * Function to set the default money-points ratio to a specific number.
 */
function _commerce_points_adjust_default_ratio($form, &$form_state) {
  // Get commerce points mode.
  $mode = $form_state['input']['commerce_points_mode'];

  // Define default order ratio.
  if ($mode == 'money') {
    $ratio = 0;
  }
  else {
    $ratio = 100;
  }

  // Define field to change.
  $field = array(
    'field_name' => 'commerce_points_money_ratio',
    'entity_type' => 'commerce_order',
    'bundle_name' => 'commerce_order',
  );

  // Fetch an instance info array.
  $instance_info = field_info_instance($field['entity_type'], $field['field_name'], $field['bundle_name']);

  // Change a single property in the instance definition.
  $instance_info['default_value'][0]['value'] = $ratio;

  // Write the changed definition back.
  field_update_instance($instance_info);

  // Set message according.
  drupal_set_message(t('The default order ratio has been set to @ratio. All new orders will start at this ratio. The leading price is "@leading_currency".', array(
    '@ratio' => $ratio,
    '@leading_currency' => $mode,
  )), 'status');
}
