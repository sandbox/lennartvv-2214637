<?php

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_points_default_rules_configuration() {
  $config = array();
  $rules_path = drupal_get_path('module', 'commerce_points') . '/includes/rules';
  $files = file_scan_directory($rules_path, '/\.rule/');
  foreach ($files as $filepath => $file) {
    require $filepath;
    if (isset($rule)) {
      $config[str_replace('.rule', '', 'rules_' . $file->name)] = rules_import($rule);
    }
  }
  return $config;
}
